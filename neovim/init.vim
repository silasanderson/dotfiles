" Enable syntax highlighting
syntax on

" Display line numbers on the left
set number

" Color
set background = "dark"

" Colorscheme
colorscheme pablo

set nocompatible
